**roll** - Roll one (or more) dice.
Syntax: `roll [-v] <rolls_number>d<dice_face>`
