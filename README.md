# oräbot v0.0.6
Hello! So this is my Discord bot. It's very simple but, well, I lilke it.

## Commands:
### Fun
> **roll** - Roll one (or more) dice.\
> Syntax: `roll [-v] <rolls_number>d<dice_face>`


### Memes
> **httrt** - WoW tout est faux...\
> Syntax: `httrt`\
> **dramastuck**\
> Syntax: `dramastuck`\
> **gifs**\
> Syntax: `konodioda`, `drift`, `yes`, `tea`, `chiale`, `fuck`, `corruption`, `nice`


### Utility
> **about** - Display information about me.\
> Syntax: `about`\
> **help** - Display command list.\
> Syntax: `help`\
> **say** - Say a message.\
> Syntax: `say <your_message>`\
> **members** - Display guild's member list.\
> Syntax: `members [-l tags] [-a all]`\
> **userinfo** - Display information about an user.\
> Syntax: `userinfo [@user]`\
> **guildinfo** - Display information about the guild where you do this command.\
> Syntax: `guildinfo`\
> **quote** - Quote a message (and its files if it has files) in an embed.\
> Syntax: `quote [channel_id]/<message_id>`\
> **embed** - Generate an embed.\
> Syntax: `embed ["title=text"] ["description=text"] ["field=name|value"] ["image_url=url"] ["thumbnail_url=url"]`
> **avatar** - Display the avatar of someone.\
> Syntax: `avatar <@someone>`
