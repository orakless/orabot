import discord
import datetime


# * maybe i should add this in my bot class, like a static method ?
def dif_date(date1, date2):
    dif = abs(date1 - date2).days
    return dif
