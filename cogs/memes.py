# oräbot
# Copyright (C) 2020 oräkle
#
# oräbot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
    Do you want some memes ? Here are every memes or private-joke commands.
"""

from discord.ext import commands
import discord


def image_embed(title, url):
    embed = discord.Embed(title=title)
    embed.set_image(url=url)
    return embed


class Memes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="httrt")
    async def httrt(self, ctx):
        await ctx.send("WoW")
        await ctx.send("tout est faux ...")

    @commands.command(name="dramastuck")
    async def dramastuck(self, ctx):
        await ctx.send("c'est lovestuck ou DRAMASTUCK??!!")

    @commands.command(name="konodioda")
    async def konodioda(self, ctx):
        await ctx.send(embed=image_embed("Kono Dio Da!",
                                         "https://media1.tenor.com/images/7c9efb6ec4909bf00517b1525a20a05d/tenor.gif"))

    @commands.command(name="promare")
    async def promare(self, ctx):
        await ctx.send(embed=image_embed("Galo Thymos!",
                                         "https://media1.tenor.com/images/368f700ed341388f12e1f791a39e1984/tenor.gif"))

    @commands.command(name="yes")
    async def yes(self, ctx):
        await ctx.send(embed=image_embed("Yes Yes Yes Yes.. Yes!",
                                         "https://media1.tenor.com/images/c4680f8a616bee0342ffa05b22c50daa/tenor.gif"))

    @commands.command(name="drift")
    async def drift(self, ctx):
        await ctx.send(embed=image_embed("Kansei Dorifuto!",
                                         "https://media1.tenor.com/images/ebc7ca2874f87495f23ef70c11a98f53/tenor.gif"))

    @commands.command(name="tea")
    async def tea(self, ctx):
        await ctx.send(embed=image_embed("\**Slurp*\*",
                                         "https://media1.tenor.com/images/05f9c9becf3fceb468034470c658b161/tenor.gif"))

    @commands.command(name="fuck")
    async def fuck(self, ctx):
        await ctx.send(embed=image_embed("Fuck you bro",
                                         "https://media.discordapp.net/attachments/433719835256553473/640631047934967866/fuck_you.gif"))

    @commands.command(name="corruption")
    async def corruption(self, ctx):
        await ctx.send(embed=image_embed("Corruption",
                                         "https://cdn.discordapp.com/attachments/433719835256553473/697921425373134929/magie_de_largent.gif"))

    @commands.command(name="nice")
    async def nice(self, ctx):
        await ctx.send(embed=image_embed("Nice!",
                                         "https://media.discordapp.net/attachments/433719835256553473/697921482554081300/nice.gif"))

    @commands.command(name="chiale")
    async def chiale(self, ctx):
        await ctx.send(embed=image_embed("Je chiale, putain!",
                                         "https://media.discordapp.net/attachments/433719835256553473/697921071831318658/je_chiale_putain.gif"))


def setup(bot):
    bot.add_cog(Memes(bot))
