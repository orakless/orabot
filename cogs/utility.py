# oräbot
# Copyright (C) 2020 oräkle
#
# oräbot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import json
import re
from random import randrange

import discord
from discord.ext import commands

import basics

with open('cogs/src/catchphrases.json') as f:
    CATCHPHRASES = json.load(f)


def get_color(roles):
    """
        it gets the colors of the highest coloured role
    :param roles: roles list
    :return: the color
    """
    color = discord.colour.Colour(0x202225)
    for role in reversed(roles):
        if role.color != discord.colour.Colour(000000):
            color = role.color
            break
    return color


def format_escape(string):
    """
        it just removes these ugly markdown characters
    :param string: string to escape
    :return: escaped string
    """
    format_string = re.sub(r'([*_~\\|`>])', r'\\\1', string)
    return format_string


def format_list(unlist, skip=False, mention=False):
    """
        It formats list. It's not the most efficient way to do it, but it's quite useful.
    :param unlist: the list to format (list)
    :param skip: do we need to skip line or not (bool)
    :param mention: do we want to add .mention to the element ? (bool)
    :return: formated list
    """

    format_string = ""
    for element in unlist:
        if isinstance(element, discord.role.Role):
            if element.name == "@everyone":
                continue
        if mention:
            format_element = element.mention
        else:
            format_element = element
        format_string = f"{format_string}{format_element}"
        if skip and unlist.index(element) != len(unlist) - 1:
            format_string = f"{format_string}\n"
        elif unlist.index(element) != len(unlist) - 1:
            format_string = f"{format_string}, "
    return format_string


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="userinfo")
    async def _userinfo(self, ctx, *users):
        """
            Displays information about someone
        """
        if len(users) < 1:
            users = (ctx.author.name,)
        for base_user in users:
            user = await commands.converter.UserConverter().convert(ctx,
                                                                    base_user)

            if isinstance(ctx.message.channel, discord.TextChannel):
                member = ctx.guild.get_member(user.id)
                userspace = "Guild"
                embed = discord.Embed(color=get_color(member.roles))
            else:
                userspace = ""
                embed = discord.Embed()
            created = datetime.date(int(user.created_at.strftime('%Y')),
                                    int(user.created_at.strftime('%m')),
                                    int(user.created_at.strftime('%d')))
            now = datetime.date(int(datetime.datetime.now().strftime('%Y')),
                                int(datetime.datetime.now().strftime('%m')),
                                int(datetime.datetime.now().strftime('%d')))

            embed.set_author(name=f"{user}",
                             icon_url=user.avatar_url)
            embed.add_field(name=f"{userspace} Nickname",
                            value=user.mention)
            embed.add_field(name="Created at", value=
            f"{str(user.created_at.strftime('%d %B %Y, %H:%M'))}\n"
            f"({basics.dif_date(now, created)} days ago)")
            if isinstance(ctx.message.channel, discord.TextChannel):
                joined = datetime.date(int(member.joined_at.strftime('%Y')),
                                       int(member.joined_at.strftime('%m')),
                                       int(member.joined_at.strftime('%d')))
                embed.add_field(name=f"Joined **{ctx.guild.name}** at",
                                value=
                                f"{str(member.joined_at.strftime('%d %B %Y, %H:%M'))}\n"
                                f"({basics.dif_date(now, joined)} days ago)")
                if len(member.roles) > 1:
                    embed.add_field(name="Roles",
                                    value=format_list(member.roles,
                                                      mention=True))
            embed.set_footer(
                text=f"!userinfo - "
                     f"Page {users.index(base_user) + 1} of {len(users)}")
            await ctx.send(embed=embed)

    @commands.command(name="about")
    async def _about(self, ctx):
        """
            Displays information about the bot and the author.
        """
        catchphrase_nb = randrange(len(CATCHPHRASES['catchphrases']))
        embed = discord.Embed(description=CATCHPHRASES['catchphrases'][catchphrase_nb]["name"],
                              color=0xffffff)
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.set_author(name=self.bot.user,
                         icon_url=self.bot.user.avatar_url)
        embed.add_field(name="Bot version",
                        value=f"v{self.bot.config['VERSION']}", inline=True)
        embed.add_field(name="Author",
                        value=f"[{self.bot.get_user(351115710992351234)}]"
                              f"(https://orakless.github.io/)",
                        inline=False)
        embed.add_field(
            name="Bot repository",
            value=f"[{self.bot.config['GIT_REPOSITORY']['name']}]"
                  f"({self.bot.config['GIT_REPOSITORY']['url']})")
        embed.add_field(name="License",
                        value=f"[{self.bot.config['LICENSE']['name']}]"
                              f"({self.bot.config['LICENSE']['url']})")
        embed.set_footer(text=f"{self.bot.command_prefix}about | Quote #{catchphrase_nb + 1} ")
        await ctx.send(embed=embed)

    @commands.command(name="help")
    async def _help(self, ctx):
        """
            Displays the help menu. The help menu is made with markdown (.md) files.
        """
        embed = discord.Embed(
            description=f"Well. You can do these commands."
                        f" The prefix is `{self.bot.command_prefix}`.")
        embed.add_field(name="__Fun__",
                        value=open("man/fun.md", "r").read(),
                        inline=False)
        embed.add_field(name="__Memes__",
                        value=open("man/memes.md", "r").read(),
                        inline=False)
        embed.add_field(name="__Utility__",
                        value=open("man/utility.md", "r").read(),
                        inline=False)
        embed.set_author(name=self.bot.user,
                         icon_url=self.bot.user.avatar_url)
        embed.set_footer(text=f"{self.bot.command_prefix}help")
        await ctx.send(embed=embed)

    @commands.command(name="say")
    async def _say(self, ctx, *, arg):
        """
            you want to insult people, but you don't want to insult them with your account,
            but if it'the bot says the insult it's ok no?
        """
        await ctx.send(arg)

    @commands.command(name="members")
    async def _members(self, ctx, *args):
        """
            Displays a complete list of members in the guild.
        """
        bot = False
        tag = False
        humans = []
        bots = []
        for argument in args:
            if argument.startswith("-"):
                if "b" in argument:
                    bot = True
                if "l" in argument:
                    tag = True
        embed = discord.Embed(title=f"Members of {ctx.guild.name}")
        embed.set_thumbnail(url=ctx.guild.icon_url)
        for member in ctx.guild.members:
            if tag:
                format_member = f"{format_escape(member.name)}" \
                                f"`#{member.discriminator}`"
            else:
                format_member = format_escape(member.display_name)
            if member.bot and not bot:
                continue
            if member.bot:
                bots.append(format_member)
            else:
                humans.append(format_member)
        embed.add_field(name=f"Humans ({len(humans)})",
                        value=format_list(humans, True))
        if bot:
            embed.add_field(name=f"Bots ({len(bots)})",
                            value=format_list(bots, True))
        await ctx.send(embed=embed)

    @commands.command(name="guildinfo", no_pm="true")
    async def _guildinfo(self, ctx):
        """
            Displays information about the guild.
        """
        print(type(ctx.message.channel))
        g = ctx.guild

        CREATED = datetime.date(int(g.created_at.strftime('%Y')),
                                int(g.created_at.strftime('%m')),
                                int(g.created_at.strftime('%d')))
        NOW = datetime.date(int(datetime.datetime.now().strftime('%Y')),
                            int(datetime.datetime.now().strftime('%m')),
                            int(datetime.datetime.now().strftime('%d')))

        embed = discord.Embed(title=g.name)
        embed.set_thumbnail(url=g.icon_url)
        embed.add_field(name="Created at", value=
        f"{str(g.created_at.strftime('%d %B %Y, %H:%M'))}\n"
        f"({basics.dif_date(NOW, CREATED)} days ago)")
        embed.add_field(name="Region", value=str(g.region))
        embed.add_field(name="Owner", value=f"<@{str(g.owner.id)}>")
        embed_list = [embed]

        if len(g.roles) >= 1 and len(
                format_list(g.roles, mention=True)) >= 1024:
            roles_embed = discord.Embed(title=f"Roles of {g.name}",
                                        description=format_list(g.roles,
                                                                mention=True))
            embed_list.append(roles_embed)
        elif len(g.roles) >= 1:
            embed.add_field(name=f"Roles of {g.name}",
                            value=format_list(g.roles, mention=True))

        if len(g.emojis) >= 1 and len(format_list(g.emojis)) >= 1024:
            emoji_embed = discord.Embed(title=f"Emojis of {g.name}",
                                        description=format_list(g.emojis))
            embed_list.append(emoji_embed)
        elif len(g.emojis) >= 1:
            embed.add_field(name=f"Emojis of {g.name}",
                            value=format_list(g.emojis))

        for item_embed in embed_list:
            await ctx.send(embed=item_embed)

    @commands.command(name="quote")
    async def _quote(self, ctx, *ids):
        """
            Quoting command. !quote [channel_id]/<message_id>
        :param ids: ids of the message to quote and the channel
        """

        for identifiant in ids:
            if len(identifiant.split("/")) == 2:
                identifiant = identifiant.split("/")
            else:
                identifiant = [ctx.channel.id, int(identifiant.split("/")[-1])]
            channel = self.bot.get_channel(int(identifiant[0]))
            message = await channel.fetch_message(int(identifiant[1]))
            embed = discord.Embed(description=message.content)
            embed.set_author(name=message.author,
                             icon_url=message.author.avatar_url)
            if message.edited_at is not None:
                footer = f"#{channel.name} - {channel.guild} | " \
                         f"{message.created_at.strftime('%d %B %Y, %H:%M')} " \
                         f"(edited: " \
                         f"{message.edited_at.strftime('%d %B %Y, %H:%M')})" \
                         f"[Go]({message.url})"
            else:
                footer = f"#{channel.name} - {channel.guild} | " \
                         f"{message.created_at.strftime('%d %B %Y, %H:%M')}"
            embed.set_footer(text=footer)
            if len(message.attachments) > 0:
                for attachments in message.attachments:
                    attachment_embed = discord.Embed(title=f"{attachments.filename}"
                                                           f" ({attachments.size} bytes)")
                    attachment_embed.set_footer(text=footer)
                    attachment_embed.set_author(name=message.author,
                                                icon_url=message.author.avatar_url)
                    if attachments.filename.endswith(".png") or \
                            attachments.filename.endswith(".jpg") or \
                            attachments.filename.endswith(".gif"):
                        if message.attachments.index(attachments) > 0:
                            attachment_embed.set_image(url=attachments.url)
                            await ctx.send(embed=attachment_embed)
                        else:
                            embed.set_image(url=attachments.url)
                            await ctx.send(embed=embed)
                    else:
                        if message.attachments.index(attachments) > 0:
                            attachment_embed.description = f"[Download]({attachments.url})"
                            await ctx.send(embed=attachment_embed)
                        else:
                            embed.add_field(name=f"{attachments.filename}"
                                                 f" ({attachments.size} bytes)",
                                            value=f"[Download]({attachments.url})")
                            await ctx.send(embed=embed)
            else:
                if len(message.content) != 0:
                    await ctx.send(embed=embed)

    @commands.command(name="embed")
    async def _embed(self, ctx, *args):
        embed = discord.Embed()
        for arg in args:
            if arg.startswith("title="):
                embed = discord.Embed(title=re.sub(r"title=(.*)", r"\1", str(arg)))
        for arg in args:
            if arg.startswith("description="):
                embed.description = re.sub(r"description=(.*)", r"\1", str(arg))
            elif arg.startswith("image_url="):
                embed.set_image(url=re.sub(r"image_url=(.*)", r"\1", str(arg)))
            elif arg.startswith("thumbnail_url="):
                embed.set_thumbnail(url=re.sub(r"thumbnail_url=(.*)", r"\1", str(arg)))
            elif arg.startswith("field="):
                field = arg.split("|")
                embed.add_field(name=re.sub(r"field=(.*)", r"\1", str(field[0])),
                                value=str(field[1]))
        if embed is not None:
            await ctx.send(embed=embed)

    @commands.command(name="avatar")
    async def _avatar(self, ctx, *members: discord.Member):
        if len(members) > 0:
            for member in members:
                embed = discord.Embed(title=f"{member.name}'s avatar",
                                      description=f"[Full size]({member.avatar_url})")
                embed.set_image(url=member.avatar_url)
                await ctx.send(embed=embed)
        else:
            embed = discord.Embed(title=f"{ctx.author.name}'s avatar",
                                  description=f"[Full size]({ctx.author.avatar_url})")
            embed.set_image(url=ctx.author.avatar_url)
            await ctx.send(embed=embed)

    @commands.command(name="fortune")
    async def _fortune(self, ctx, *args):
        if len(args) == 0:
            catchphrase_nb = randrange(len(CATCHPHRASES['catchphrases']))
            embed = discord.Embed(title=f"Quote #{catchphrase_nb + 1}",
                                  description=CATCHPHRASES['catchphrases'][catchphrase_nb]["name"])
            embed.set_footer(text=
                             f"{CATCHPHRASES['catchphrases'][catchphrase_nb]['character']} | "
                             f"{CATCHPHRASES['catchphrases'][catchphrase_nb]['serie']}")
            await ctx.send(embed=embed)
        else:
            for arg in args:
                try:
                    if int(arg) <= 0:
                        arg = len(CATCHPHRASES['catchphrases']) + int(arg)
                        if int(arg) <= 0:
                            raise IndexError
                    embed = discord.Embed(title=f"Quote #{arg}",
                                          description=CATCHPHRASES['catchphrases'][int(arg) - 1][
                                              "name"])
                    embed.set_footer(text=
                                     f"{CATCHPHRASES['catchphrases'][int(arg) - 1]['character']}"
                                     f" | {CATCHPHRASES['catchphrases'][int(arg) - 1]['serie']}")
                    await ctx.send(embed=embed)
                except IndexError:
                    embed = discord.Embed(title=f"Error",
                                          description=f"Index {arg} out of range")
                    await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Utility(bot))
