# oräbot
# Copyright (C) 2020 oräkle
#
# oräbot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import discord
from discord.ext import commands
from random import randrange
import basics


class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="roll")
    async def _roll(self, ctx, *args):
        errors = 0
        embed = discord.Embed(description=f"Eyuup, I just rolled the dice!",
                              color=0xffffff)
        embed.set_author(name=self.bot.user,
                         icon_url=self.bot.user.avatar_url)
        embed.set_footer(text=f"{self.bot.command_prefix}roll")
        verbose = False
        for i in range(len(args)):
            if args[i] == "-v":
                verbose = True
        for i in range(len(args)):
            try:
                if args[i] != "-v":
                    results = []
                    total = 0
                    dice = args[i].split("d")

                    if int(dice[0]) > 100000000:
                        raise Exception("Too much dice. I don't have enough robotic hands to roll them.")

                    if int(dice[0]) <= 0 or int(dice[1]) <= 0:
                        raise Exception("Numbers must be greater than 0.")

                    if len(dice) > 2:
                        raise Exception("Too many arguments.")

                    for e in range(int(dice[0])):
                        results.append(randrange(int(dice[1])) + 1)

                    if int(dice[1]) > 1:
                        for d in range(len(results)):
                            total += results[d]
                    else:
                        total = dice[0]

                    if verbose and len(str(results)) < 1024:
                        embed.add_field(name=f"{args[i]} (total = {total})",
                                        value=f"{results}",
                                        inline=True)
                    else:
                        embed.add_field(name=f"{args[i]}",
                                        value=f"{total}",
                                        inline=True)
            except Exception as e:
                errors += 1
                embed = basics.send_error(ctx, e, errors)
                if embed is not None:
                    await ctx.send(embed=embed)
        if len(embed.fields) > 0:
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Fun(bot))
