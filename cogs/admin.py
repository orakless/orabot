# oräbot
# Copyright (C) 2020 oräkle
#
# oräbot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from discord.ext import commands
import discord


def has_permissions(self, ctx):
    if int(ctx.message.author.id) == 351115710992351234:
        return True
    else:
        return False


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="is_owner")
    async def is_owner(self, ctx):
        if has_permissions(self, ctx):
            await ctx.send("You are the owner of this bot!")
        else:
            await ctx.send("Wait.. you're not the owner, right ?")

    @commands.command(name="ora_servers")
    async def ora_servers(self, ctx):
        if has_permissions(self, ctx):
            embed = discord.Embed(title="Guilds where I am", description="")
            guilds = await self.bot.fetch_guilds(limit=150).flatten()
            for server in guilds:
                print(server)
                embed.description = f"{embed.description}{server.name}\n"
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Admin(bot))
