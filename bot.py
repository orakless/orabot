# oräbot
# Copyright (C) 2020 oräkle
#
# oräbot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
    Hello! This is the main file for orabot. That's where you launch the bot.
"""

import datetime
import json
import logging

import re
import discord
from discord.ext import commands
from sty import fg

# AM JUST LOADING THE CONFIG FILE
with open('config.json') as f:
    CONFIG = json.load(f)
# VARIABLES/CONSTS
NOW = datetime.datetime.now()
GRAY = fg(125, 125, 125)
# LOGS
SEPARATOR = f"{fg.blue}---------------------------------------------------" \
            f"{fg.rs}"
LOGGER = logging.getLogger("discord")
LOGGER.setLevel(logging.DEBUG)
HANDLER = logging.FileHandler(
    filename=f"{CONFIG['LOGS']['path']}{CONFIG['LOGS']['name']}"
             f"{NOW.strftime('%Y-%m-%d_%H-%M')}.log",
    encoding="utf-8",
    mode="w")
HANDLER.setFormatter(
    logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
LOGGER.addHandler(HANDLER)
# COGS
COG_LIST = (
    'cogs.utility',
    'cogs.memes',
    'cogs.fun',
    'cogs.admin'
)


def launch_message(verb, self):
    """
        This is the launch/update message. When this is displayed, the bot
        is alive.
    :param verb: verb = launching or updated. not used right now.
    :param self: its the bot parameter
    :return: It returns nothing.
    """
    print(
        f"- {self.user.name} discord bot "
        f"v{fg.red}{self.config['VERSION']}{fg.rs}")
    print(
        f"- Bot {verb}. "
        f"{fg.red}{self.user}{fg.rs} ({fg.red}{self.user.id}{fg.rs})")
    print(f"- Prefix: {fg.red}{self.command_prefix}{fg.rs}")
    print(f"- Git repo: "
          f"{fg.red}{self.config['GIT_REPOSITORY']['url']}{fg.rs}")
    print(
        f"- This bot is licensed under "
        f"{fg.red}{self.config['LICENSE']['short']}{fg.rs}."
        f" If you like it, contribute!")
    print(SEPARATOR)


def load_cogs(self):
    """
        It's the function which loads cogs. It also displays errors if a cog
        could not be loaded.
    :param self: the bot
    :return: nothing
    """
    print("Loading cogs...")
    for ext in COG_LIST:
        self.load_extension(ext)
        print(f"- {fg.red}{ext}{fg.rs} loaded.")
    print("Cogs loaded!")
    print(SEPARATOR)


class Orabot(commands.Bot):
    """
        It's the bot's class. I need it because of the cogs, and that's a
        nice method to declare a bot.
    """
    def __init__(self):
        print("Launching the bot...")
        print(SEPARATOR)
        super().__init__(command_prefix=CONFIG['PREFIX'],
                         description=CONFIG['DESCRIPTION'],
                         help_command=None)
        self.config = CONFIG
        self.activity = discord.Activity(type=discord.ActivityType.playing,
                                         name="RUN>>>")
        self.status = discord.Status.dnd
        load_cogs(self)

    async def on_message(self, message):
        """
            It's the public method that occurs when we receive a message.
            I just added a regexp, that's a private joke.
        """
        if message.author.bot:
            return

        if re.search(r"m+\s*d+\s*r+\s*x\d+", message.content.lower()):
            try:
                await message.delete()
            except discord.errors.Forbidden:
                await message.channel.send(
                    "Hi.. I lack some permissions... I need the `Manage Messages` permission to delete your 'mdr'. :disappointed:"
                    f"{message.jump_url}")

        if "muda" in message.content.lower():
            muda_embed = discord.Embed(title="I can't beat the shit out of you without getting "
                                             f"closer, {message.author.name}")
            muda_embed.set_image(
                url="https://media1.tenor.com/images/fd95dd99e198ba5f984949428012f8a4/tenor.gif")
            await message.channel.send(embed=muda_embed)

        await self.process_commands(message)

    async def on_message_edit(self, before, after):
        if after.author.bot:
            return

        if re.search(r"m+\s*d+\s*r+\s*x\d+", after.content.lower()):
            try:
                await after.delete()
            except discord.errors.Forbidden:
                await after.channel.send(
                    "Hi.. I lack some permissions... I need the `Manage Messages` permission to delete your 'mdr'. :disappointed:\n"
                    f"{after.jump_url}")

    async def on_ready(self):
        """
            Things that occurs when the bot is online.
        """
        launch_message("launched", self)
        await self.change_presence(status=self.status,
                                   activity=self.activity)

    async def on_command_error(self, context, exception):
        """
            Occurs when a command raise an exception. Useful for logging
            purpose.
        """
        if isinstance(exception, commands.NoPrivateMessage):
            error_embed = discord.Embed(title="Error",
                                        description="You can't use this "
                                                    "command in private "
                                                    "messages.")
            await context.channel.send(embed=error_embed)
        else:
            print(f"{GRAY}[{NOW.strftime('%H:%M')}]{fg.rs}"
                  f" {context.guild.name}, "
                  f"#{context.channel.name}:\n"
                  f"{str(exception)}\n"
                  f"Command: {context.message.content}")
            print(SEPARATOR)

    async def on_disconnect(self):
        print(f"Disconnected {datetime.datetime.now().strftime('%H:%M')}\n{SEPARATOR}")


BOT = Orabot()

BOT.run(BOT.config["TOKEN"], reconnect=True)
